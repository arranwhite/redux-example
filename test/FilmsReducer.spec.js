import { expect } from 'chai';
import { films } from '../reducers/reducers';
import * as types from '../actions/ActionTypes';

describe('Reducers', () => {
    describe('films', () => {
        it('should set films to films from response when RECV_FILM fired', () => {
            const oldAppState = [];
            const testFilms = [
              { title: 'A New Hope' },
              { title: 'Empire Strikes Back' }
            ];
            const recvFilmsAction = { type: types.RECV_FILMS, films: testFilms };

            const newFilmsState = films(oldAppState, recvFilmsAction);
            expect(newFilmsState).to.have.lengthOf(2);
            expect(newFilmsState[0].title).to.equal('A New Hope');
            expect(newFilmsState[1].title).to.equal('Empire Strikes Back');
        });
    });
});
