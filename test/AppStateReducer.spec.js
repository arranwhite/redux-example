import { expect } from 'chai';
import { appState } from '../src/reducers/reducers';
import * as types from '../src/actions/ActionTypes';

describe('Reducers', () => {
    describe('appState', () => {
        it('should set inProgress to true when REQ_FILMS action fired', () => {
            const oldAppState = {};
            const requestFilmsAction = { type: types.REQ_FILMS };

            const newAppState = appState(oldAppState, requestFilmsAction);
            expect(newAppState.inProgress).to.be.true;
        });

        it('should set inProgress to false when RECV_FILMS action fired', () => {
            const oldAppState = {};
            const recvFilmsAction = { type: types.RECV_FILMS, films: [] };

            const newAppState = appState(oldAppState, recvFilmsAction);
            expect(newAppState.inProgress).to.be.false;
        });
    });
});
