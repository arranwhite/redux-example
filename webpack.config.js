var path = require('path');
var webpack = require('webpack');

module.exports = {
    context: __dirname,
    entry: './src/index.js',
    output: {
        path: __dirname,
        filename: 'bundle.js'
    },
    module: {
      loaders: [
        { test: /\.js$/, loader: 'babel', exclude: path.join(__dirname, 'node_modules') },
        { test: /\.scss$/, loaders: ["style", "css", "sass"] }
      ]
    }
}
